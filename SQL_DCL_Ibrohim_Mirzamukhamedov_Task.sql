CREATE USER rentaluser WITH PASSWORD 'rentalpassword';
REVOKE ALL PRIVILEGES ON dvdrental FROM rentaluser;
GRANT CONNECT ON dvdrental TO rentaluser;
GRANT SELECT ON customer TO rentaluser;

SET ROLE rentaluser;

SELECT * FROM customer; --work

SELECT * FROM another_table; -- assuming another table exists

RESET ROLE;

CREATE GROUP rental_group;

ALTER USER rentaluser ADD TO GROUP rental_group;

GRANT INSERT, UPDATE ON rental TO rental_group;

SET ROLE rental_group;

INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id)
VALUES ('2024-04-07 14:00:00', 1, 23, '2024-04-30 15:00:00', 2);

UPDATE rental
SET return_date = '2024-04-07 14:00:00'
WHERE rental_id = 90000;

RESET ROLE;

REVOKE INSERT ON rental FROM rental_group;

SET ROLE rental_group;

INSERT INTO rental (rental_date, inventory_id, customer_id, return_date, staff_id)
VALUES ('2024-04-28 15:00:00', 1, 23, '2024-04-30 15:00:00', 2);

RESET ROLE;

-- Find a customer with data (anonymized)
SELECT customer_id, any_non_identifiable_column
FROM customer
JOIN rental USING (customer_id)
JOIN payment USING (customer_id)
WHERE rental_id IS NOT NULL AND payment_id IS NOT NULL
LIMIT 1;
 
-- Use a generic role name
CREATE ROLE client_John_Doe;

GRANT USAGE ON SCHEMA public TO client_John_Doe;
GRANT SELECT ON rental, payment TO client_John_Doe;

-- Enable Row Level Security on payment and rental (implementation details omitted)

ALTER TABLE payment ENABLE ROW LEVEL SECURITY;
ALTER TABLE rental ENABLE ROW LEVEL SECURITY;

-- Policy definition for Row Level Security omitted (likely filters by customer_id)

ALTER TABLE rental FORCE ROW LEVEL SECURITY;
ALTER TABLE payment FORCE ROW LEVEL SECURITY;

SET ROLE client_John_Doe;

SELECT * FROM rental;
SELECT * FROM payment;

RESET ROLE;

